# Introduction

a comprehensive program focused on rendering scenes through ray tracing techniques. This application centers on a world defined by a single cube, leveraging fundamental computer graphics principles to achieve visual realism. This implementation encapsulates essential components, including camera setup, lighting, shading, and interaction.

At its core, the program employs ray tracing to simulate light interactions within the confined cube-based environment. The code initializes key elements such as the camera's position, lighting conditions, and object definitions. Noteworthy features encompass the handling of keyboard inputs for camera movement, rendering pixel colors, computing indirect illumination effects, shadow casting, and optimization through multi-threading. The rendering process adheres to a custom photon mapping approach, enhancing the realism of the depicted scenes. The integration of OpenGL functionalities enables graphical output and user interaction.

The program's objective is to produce visually compelling renderings by accurately simulating the complex interplay of light and surfaces within the specified cubic world. This code serves as an educational illustration of ray tracing techniques and their application to a minimalistic graphical setting, contributing to a deeper understanding of computer graphics concepts and algorithms.



# RUN
./build/rtr  --obj-file  ./objs/monkey.ob



