# Introduction  

### Montelight

This program is a ray tracer based on the Monte Carlo path tracing algorithm, used to render realistic images. It simulates the propagation of rays in a 3D scene, calculating ray intersections with various geometric objects in the scene and interactions with lighting to generate the final rendered image.

Here's an overview of what the program does and its flow:

Header Files and Global Variables: The program begins by including necessary header files and defining some global constants and data structures such as 3D vectors (Vector), rays (Ray), images (Image), shapes (Shape), etc.

Vector and Ray Definitions: It defines classes to represent vectors and rays, along with overloaded operators and methods for vector operations like addition, subtraction, multiplication, division, dot product, normalization, etc.

Image Definition: It defines a class to represent an image, including its width, height, pixel data, etc. Methods are provided for setting and retrieving pixel colors.

Shape Definitions: It defines a base class Shape for different geometric objects (e.g., spheres) and its subclass Sphere. Each shape has properties like color, emissive light, and methods to calculate intersections, normals, and random points.

Scene Definitions: The program defines two scenes, simpleScene and complexScene, each composed of multiple shapes to describe the rendering scenes.

Ray Tracer Definition: It defines the Tracer class that takes scene data and implements methods for calculating ray intersections with various geometric objects in the scene, lighting computations, reflection calculations, etc.

Main Function: In the main function, an image object is created based on settings. Using the scene data and camera setup, the Monte Carlo path tracing algorithm is applied for multiple samples. For each iteration, every pixel is sampled to generate rays and calculate intersections and lighting. After repeated sampling, the accumulated rendering results for each pixel are obtained.

Sampling and Rendering Process: Through iterations, each pixel is sampled multiple times. In each iteration, rays are generated for each pixel, intersections are calculated, and lighting is computed. Repeated sampling accumulates the results, generating a final rendered image.

The program employs techniques such as reflection, diffuse reflection, shadows, light source sampling, etc., to simulate the propagation and interaction of rays in the scene, resulting in the generation of realistic rendered images. Parameters in the program can be adjusted to control aspects like sample count, focal effects, scene setup, etc., affecting the quality and appearance of the final rendering. 



![](http://smerity.com/montelight-cpp/img/montage_passes_annotated.png)


# RUN 

To compile, use any C++11 compatible compiler such as `g++` or `clang++`:

    g++ -O3 -std=c++0x montelight.cc -o montelight
    clang++ -O3 -std=c++0x montelight.cc -o montelight

To run the program, simply execute it on the command line via:
    ./montelight

If you provide a `temp` folder, a number of in progress renders will be saved.
The final result will be saved as `render.ppm`.

For modifying the parameters of the program, refer to the annotated code overview.
