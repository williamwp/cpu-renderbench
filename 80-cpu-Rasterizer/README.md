# Introduction 

A CPU rasterizer is a software-based graphics rendering technique that focuses on generating images from 3D scene data using the CPU (Central Processing Unit) of a computer. It's a fundamental approach employed in computer graphics to transform 3D geometry and scene data into 2D images displayed on a screen. Unlike GPU (Graphics Processing Unit) rendering, which offloads rendering tasks to specialized hardware, CPU rasterization relies solely on the computational power of the CPU.

Here's an overview of the CPU rasterization process:

Vertex Processing: The CPU rasterizer begins by transforming 3D vertex data from the 3D world coordinates into 2D screen-space coordinates. This transformation involves applying transformations like translation, rotation, and scaling to each vertex.

Primitive Assembly: The CPU rasterizer groups vertices into geometric primitives, such as triangles, lines, or points, forming the building blocks for rendering.

Clipping: Geometric primitives that extend beyond the view frustum are clipped to ensure they fit within the visible screen area.

Screen Mapping: The 3D coordinates of the clipped primitives are then projected onto the 2D screen space, converting the 3D scene into a 2D representation.

Rasterization: The CPU rasterizer determines which pixels on the screen are covered by each primitive. It computes the screen coordinates of the pixels inside the primitive using techniques like scanline rasterization or bounding box tests.

Interpolation and Shading: Color attributes, such as texture coordinates and lighting information, are interpolated across the primitive's surface for each pixel. These interpolated values contribute to the final color of the pixel.

Fragment Processing: The interpolated values are used in fragment processing to determine the final color of each pixel. This may involve applying lighting models, textures, and other effects to produce the pixel's color.

Framebuffer Update: The computed pixel colors are written to a framebuffer, which represents the final image that will be displayed on the screen.

Display: The resulting framebuffer is sent to the display hardware for presentation on the screen.

CPU rasterization has several advantages, including ease of implementation and full control over the rendering process. However, it can be less efficient than GPU rendering, particularly for complex scenes, due to the CPU's limited parallel processing capabilities. Modern graphics rendering often relies on a combination of CPU and GPU techniques to achieve optimal performance and visual quality.

# RUN  
./rasterizer  

