

#ifndef CudaRasterizer_mesh_loader_h
#define CudaRasterizer_mesh_loader_h

#include "structures.h"

void load_m_mesh(mesh_t *mesh, const char *meshFile);

#endif
