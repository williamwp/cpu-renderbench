

#ifndef CudaRasterizer_png_loader_h
#define CudaRasterizer_png_loader_h

#include "structures.h"

int save_png_to_file(bitmap_t *bitmap, const char *path);

#endif
