# Introduction

The library's highlighted features encompass a seamless, single-file implementation of a programmable rendering pipeline starting from point rendering, without external dependencies. It adheres to model standards, ensuring precise calculations, and offers a familiar interface akin to Direct3D. The library includes comprehensive vector/matrix libraries and a simplified bitmap library for tasks like rendering points, lines, and texture manipulation. Vertex and pixel shaders can be authored in C++, aiding debugging through breakpoints. The Edge Equation is employed for accurate triangle coverage calculation, considering border cases for adjacent triangles. Barycentric coordinates formula is adeptly used for varying interpolation, while 1/w correction ensures faithful perspective-correct texture rendering. Rendering quality is enhanced using quadratic linear interpolation. The core rendering, just 200 lines, is designed for clarity and supported by thorough Chinese commentary. Educational examples span from basic triangle rendering to advanced topics such as 3D modeling and lighting. Compilation involves using gcc for 'sample_'-prefixed files, with potential '-std=c++17' inclusion and '-lm' for math library linking on specific platforms.
# RUN

```bash
./a1
```
