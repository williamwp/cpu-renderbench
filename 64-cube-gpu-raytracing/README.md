# Introduction

### cube-raytracing
In the realm of computer graphics, ray tracing takes on a novel dimension when applied to a world exclusively composed of a single cube. This minimalist environment serves as a unique canvas to explore the intricacies of ray-tracing techniques. By casting rays from a virtual camera into this constrained space, interactions between the rays and the cube's surfaces yield captivating visual outcomes. Reflective, refractive, and shadow effects are all distilled to their essence within this confined world, offering a focused arena to study how light behaves in the context of geometric simplicity. This scenario provides an opportunity to investigate how rays intersect, bounce, and interact with a solitary object, offering insights into fundamental concepts like surface shading, lighting, and perspective. By embracing the limitations of this simplified world, ray tracing within a single cube opens a captivating avenue for grasping the core principles of ray-tracing technology while producing visually intriguing results.

## Result

### Render

256 Samples : 

![256 samples](256s.bmp)

### Performance

* CPU
  - CPU(s): 40
  - Thread(s) per core: 2
  - Core(s) per socket: 10
  - Socket(s): 2

  
#### CPU Threading

| Samples\Threading | 1 | 2 | 4 | 8 | 16 |
|:--:|-----:|-----:|-----:|-----:|-----:|
| 1  | 1.42 | 0.82 | 0.44 | 0.25 | 0.21 |
| 4  | 5.28 | 2.74 | 2.08 | 1.11 | 0.62 |
| 9  | 11.95| 6.27 | 3.45 | 1.81 | 1.29 |
|16  | 21.00|11.41 | 5.96 | 4.28 | 2.22 |
|25  | 32.46|18.50 | 9.64 | 4.72 | 3.44 |

