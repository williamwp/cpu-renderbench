# Introduction

This program primarily implements a simple graphics rendering application. It utilizes the provided classes and shapes to create a visual scene that includes spheres, a torus, and a cone, each with distinct properties. The code sets up a graphics window, populates a scene with these shapes, adds lighting, and configures a camera's viewpoint to render the resulting scene. In summary, the program creates a basic graphical representation with geometric shapes, lighting, and a camera perspective, demonstrating the fundamentals of computer graphics rendering.

# RUN 
./aaa 


