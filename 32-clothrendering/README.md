# Introduction

The implementation comprises correct [5points] formatting, allowing for successful compilation and execution, as depicted in the results shown in the /images directory.

The PathTracing algorithm [45points] has been accurately realized, predominantly following the algorithmic approach from the instructor's presentation slides. Several key points necessitate attention, including ensuring the proper handling of directions for 'ws' and '-ws', addressing light source considerations to prevent instances of complete blackness, and acknowledging that 'wo' corresponds to the negative direction of the ray. In a single-threaded environment, the execution time was approximately 68 minutes. Though specific results were inadvertently cleared, subsequent runs were deemed too time-consuming for replication.

The submitted result images showcase the algorithm's effectiveness in various scenarios, including 784784 with 16 point samples and 512512 with 8 point samples, alongside additional rapid verification tests.

The multi-threading component [10points] was incorporated despite personal unfamiliarity with C++ multi-threading. After researching discussions on forums and utilizing online resources, the OpenMP approach outlined in https://blog.csdn.net/fengtian12345/article/details/80566491 was adopted. Due to limited hardware resources (a dual-core CPU in a MacBook Pro 2015 Early version), multi-threading demonstrated modest enhancements, reducing execution time to around 60 minutes. Higher sample point counts were not explored. Notably, when implementing multi-threading, the progress bar loading, previously executed only once, needed separate handling.

# RUN 
./build/RayTracing

