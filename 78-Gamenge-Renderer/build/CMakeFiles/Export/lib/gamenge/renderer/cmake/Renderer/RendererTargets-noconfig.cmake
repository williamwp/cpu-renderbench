#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "Gamenge::Renderer" for configuration ""
set_property(TARGET Gamenge::Renderer APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(Gamenge::Renderer PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_NOCONFIG "CXX"
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/gamenge/renderer/librenderer.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS Gamenge::Renderer )
list(APPEND _IMPORT_CHECK_FILES_FOR_Gamenge::Renderer "${_IMPORT_PREFIX}/lib/gamenge/renderer/librenderer.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
